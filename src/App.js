import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import Homepage from './Pages/Homepage';
import CartPage from './Pages/CartPage';
import FavoritesPage from './Pages/FavoritesPage';
import Header from './components/Header';
import './components/Cards.scss';

export const ViewModeContext = React.createContext();

const App = () => {
  const [viewMode, setViewMode] = useState('cards');

  const toggleViewMode = () => {
    setViewMode(prevMode => (prevMode === 'cards' ? 'table' : 'cards'));
  };

  return (
    <Router>
      <ViewModeContext.Provider value={{ viewMode, toggleViewMode }}>
        <Header />
        <div>
          <nav className='nav-container'>
            <ul className='nav-options'>
              <li className='nav-items'><Link to="/">Main Page</Link></li>
              <li className='nav-items'><Link to="/cart">Cart</Link></li>
              <li className='nav-items'><Link to="/favorites">Favorites</Link></li>
            </ul>
          </nav>
          <Routes>
            <Route
              path="/"
              element={<Homepage />}
            />
            <Route
              path="/cart"
              element={<CartPage />}
            />
            <Route
              path="/favorites"
              element={<FavoritesPage />}
            />
          </Routes>
        </div>
      </ViewModeContext.Provider>
    </Router>
  );
};

export default App;
