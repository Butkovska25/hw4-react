import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setProducts, openModal, closeModal, addToCart, toggleFavorite } from '../redux/actions';
import ProductList from '../components/ProductList';
import Modal from '../components/Modal';

const Homepage = () => {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products);
  const isModalOpen = useSelector((state) => state.isModalOpen);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const favorites = useSelector((state) => state.favorites);
  const handleToggleFavorite = (productId) => {
    dispatch(toggleFavorite(productId));};
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('/products.json');
        const data = await response.json();
        dispatch(setProducts(data));
      } catch (error) {
        console.error('Помилка завантаження', error);
      }};
    fetchData();
  }, [dispatch]);
  const handleAddToCart = (product) => {
    setSelectedProduct(product);
    dispatch(openModal());};

  const handleConfirmAddToCart = () => {
    if (selectedProduct) {
      dispatch(addToCart(selectedProduct)); 
      dispatch(closeModal());
      setSelectedProduct(null);
    }};

  return (
    <div>
      <ProductList products={products} addToCart={handleAddToCart}  favorites={favorites} toggleFavorite={handleToggleFavorite}/>
      {isModalOpen && (
        <Modal
          onConfirm={handleConfirmAddToCart}
          onCancel={() => {
            dispatch(closeModal());
            setSelectedProduct(null);
          }}
        />
      )}
    </div>
  );
};

export default Homepage;
