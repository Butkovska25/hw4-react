import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { removeFromCart } from '../redux/actions';
import Modal from '../components/Modal';
import OrderForm from '../components/OrderForm';

const CartPage = () => {
  const dispatch = useDispatch();
  const cartItems = useSelector(state => state.cart);
  const [itemToDelete, setItemToDelete] = useState(null);
  const handleRemoveFromCart = (productId) => {
    dispatch(removeFromCart(productId));
    setItemToDelete(null);
  };

  return (
    <div>
      <h1>Корзина</h1>
      <div className='possition'>
      <ul className='product-list'>
      {cartItems.map(item => (
      <li key={item.id}>
      <div className='product-card'>
      <img className="product-photo" src={item.imageUrl} alt={item.name} />
      <div className='card-text'>
      <h3>{item.name}</h3>
      <strong>{item.title}</strong>
      <p>Ціна: ${item.price}</p> </div>
      <p>{`Article: ${item.article}`}</p>
      <p>{`Color: ${item.color}`}</p>
      <button className="button-add-to-cart" onClick={() => setItemToDelete(item.id)}>Видалити</button>
      </div>
      </li>
        ))}</ul>
      {itemToDelete && (
        <Modal
          onConfirm={() => handleRemoveFromCart(itemToDelete)}
          onCancel={() => setItemToDelete(null)}
        />
      )}
      <OrderForm /></div>
    </div>
  );
};

export default CartPage;
