import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toggleFavorite } from '../redux/actions';

const FavoritesPage = () => {
  const dispatch = useDispatch();
  const products = useSelector(state => state.products);
  const favorites = useSelector(state => state.favorites);
  const handleToggleFavorite = (productId) => {
    dispatch(toggleFavorite(productId));
  };

  return (
    <div>
      <h1>Улюблені товари</h1>
      <ul className='product-list'>
        {favorites.map(productId => {
          const product = products.find(p => p.id === productId);
          if (!product) return null;
          return (
            <li key={product.id}>
            <div className='product-card'>
            <img className="product-photo" src={product.imageUrl} alt={product.name} />
      <div className='card-text'>
      <h3>{product.name}</h3>
      <strong>{product.title}</strong>
      <p>Ціна: ${product.price}</p> </div>
      <p>{`Article: ${product.article}`}</p>
      <p>{`Color: ${product.color}`}</p>
      <button className="button-add-to-cart" onClick={() => handleToggleFavorite(productId)}>Видалити з улюблених</button>
      </div></li>
          );
        })}
      </ul>
    </div>
  );
};

export default FavoritesPage;
