import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Header = () => {
  const cartItems = useSelector(state => state.cart);
  const favorites = useSelector(state => state.favorites);

  return (
    <header>
      <nav>
        <ul>
          <li>
            <Link to="/cart">
              <span role="img" aria-label="cart">🛒</span> Кошик ({cartItems.length})
            </Link>
          </li>
          <li>
            <Link to="/favorites">
              <span role="img" aria-label="favorites">❤️</span> Улюблені ({favorites.length})
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
