import React from 'react';
import './modal.scss';

const Modal = ({ onConfirm, onCancel }) => {
  return (
    <div className="modal-overlay" onClick={onCancel}> 
      <div className="modal-content">
      {onCancel && (<span className="close-btn" onClick={onCancel}>&#x2715;</span>)}

        <p className='message'>Підтвердіть вашу дію</p>
        <div className="modal-buttons">
          <button className="modal-button confirm" onClick={onConfirm}>Підтвердити</button>
          <button className="modal-button cancel" onClick={onCancel}>Скасувати</button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
