import React, { useState } from 'react';
import ProductItem from './ProductItem';
import PropTypes from 'prop-types';

const ProductList = ({ products, addToCart, favorites, toggleFavorite }) => {
  const [viewMode, setViewMode] = useState('cards');

  const toggleViewMode = () => {
    setViewMode(prevMode => (prevMode === 'table' ? 'cards' : 'table'));
  };

  return (
    <div>
      <h1>Список продуктів</h1>
      <button className='button-mode' onClick={toggleViewMode}>
        {viewMode === 'cards' ? 'List mode' : 'Table mode'}
      </button>
      <ul className={viewMode === 'cards' ? 'product-list-table' : 'product-list-cards'}>
        {products.map((product) => (
          <li key={product.id}>
            <ProductItem product={product} addToCart={addToCart} isFavorite={favorites.includes(product.id)} toggleFavorite={toggleFavorite}/>
          </li>
        ))}
      </ul>
    </div>
  );
};

ProductList.propTypes = {
  addToCart: PropTypes.func.isRequired,
  toggleFavorite: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired,
  favorites: PropTypes.array.isRequired,
};

export default ProductList;