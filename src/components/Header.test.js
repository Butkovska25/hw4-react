import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import Header from './Header';
import store from '../redux/store';

describe('Header component', () => {
  it('matchs snapshot', () => {
    const { asFragment } = render(
      <Provider store={store}>
        <Router>
          <Header />
        </Router>
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });

  it('shows items in cart and favorites', () => {
    const { getByLabelText } = render(
      <Provider store={store}>
        <Router>
          <Header />
        </Router>
      </Provider>
    );
    const cartLink = getByLabelText(/cart/i);
    const favoritesLink = getByLabelText(/favorites/i);
    
    expect(cartLink.textContent).toContain('🛒');
    expect(favoritesLink.textContent).toContain('❤️');
  });
});
