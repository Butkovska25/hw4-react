import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Modal from './Modal';

describe('Modal component', () => {
  test('renders without crashing', () => {
    render(<Modal />);
  });

  test('onConfirm callback when clicking on confirm button', () => {
    const onConfirm = jest.fn();
    const { getByText } = render(<Modal onConfirm={onConfirm} />);
    const confirmButton = getByText('Підтвердити');
    fireEvent.click(confirmButton);
    expect(onConfirm).toHaveBeenCalled();
  });

  test('onCancel callback when clicking on cancel button', () => {
    const onCancel = jest.fn();
    const { getByText } = render(<Modal onCancel={onCancel} />);
    const cancelButton = getByText('Скасувати');
    fireEvent.click(cancelButton);
    expect(onCancel).toHaveBeenCalled();
  });
});