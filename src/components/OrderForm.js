import React, { useState } from 'react'; 
import { useDispatch, useSelector } from 'react-redux';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { clearCart } from '../redux/actions';

const OrderForm = () => {
  const dispatch = useDispatch();
  const cartItems = useSelector(state => state.cart);
  const [orderPlaced, setOrderPlaced] = useState(false);
  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required('Обов\'язкове поле').matches(/^[A-Za-zА-Яа-яЁёіІїЇєЄґҐ]*$/, 'Формат введення: текст'),
    lastName: Yup.string().required('Обов\'язкове поле').matches(/^[A-Za-zА-Яа-яЁёіІїЇєЄґҐ]*$/, 'Формат введення: текст'),
    age: Yup.number().required('Обов\'язкове поле').min(18, 'Мінімальний вік 16 років'),
    address: Yup.string().required('Обов\'язкове поле'),
    phone: Yup.string().required('Обов\'язкове поле').matches(/^[0-9]+$/, 'Формат введення: числа'),
  });

  const handleSubmit = async (values, { setSubmitting }) => {
    try {
      const purchasedItems = cartItems;
      dispatch(clearCart());
      console.log('Інформація про замовника:', values);
      console.log('Придбані позиції:', purchasedItems);
      setOrderPlaced(true);
    setSubmitting();
    } catch (error) {
      console.error('Помилка замовлення:', error);
    }
    setSubmitting(false);
  };

  return (
    <div className="form-container">
      <h2>Дані про замовника</h2>
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          age: '',
          address: '',
          phone: '',
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting }) => (
          <Form>
            <div className="form-group">
              <label htmlFor="firstName" className="label">Ім'я замовника</label>
              <Field type="text" name="firstName" className="input-field"/>
              <ErrorMessage name="firstName" component="div" className="error-message"/>
            </div>
            <div className="form-group">
              <label htmlFor="lastName" className="label">Прізвище замовника</label>
              <Field type="text" name="lastName" className="input-field"/>
              <ErrorMessage name="lastName" component="div" className="error-message"/>
            </div>
            <div className="form-group">
              <label htmlFor="age" className="label">Вік</label>
              <Field type="number" name="age" className="input-field"/>
              <ErrorMessage name="age" component="div" className="error-message"/>
            </div>
            <div className="form-group">
              <label htmlFor="address" className="label">Адреса доставки замовлення</label>
              <Field type="text" name="address" className="input-field"/>
              <ErrorMessage name="address" component="div" className="error-message"/>
            </div>
            <div className="form-group">
              <label htmlFor="phone" className="label">Мобільний телефон</label>
              <Field type="text" name="phone" pattern="[0-9]*" className="input-field"/>
              <ErrorMessage name="phone" component="div" className="error-message"/>
            </div>
            <button className="button-add-to-cart" type="submit" disabled={isSubmitting}>Checkout</button>
            {orderPlaced && <p className='order-text'>💌 Дякуємо, Ваше замовлення прийнято 💌</p>}
          </Form>  )}
      </Formik>
    </div>
  );
};

export default OrderForm;