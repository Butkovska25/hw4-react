const initialState = {
  products: [],
  cart: [],
  favorites: [],
  isModalOpen: false,
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_PRODUCTS':
    return { ...state, products: action.payload };
    case 'ADD_TO_CART':
    return { ...state, cart: [...state.cart, action.payload] };
    case 'REMOVE_FROM_CART':
          const productIdToRemove = action.payload;
    return {
            ...state,
            cart: state.cart.filter(item => item.id !== productIdToRemove)
          };
    case 'OPEN_MODAL':
    return { ...state, isModalOpen: true };
    case 'CLOSE_MODAL':
    return { ...state, isModalOpen: false };
    case 'TOGGLE_FAVORITE':
        const productId = action.payload;
        const index = state.favorites.indexOf(productId);
        if (index !== -1) {
    return { ...state, favorites: state.favorites.filter(id => id !== productId) };
        } else {
    return { ...state, favorites: [...state.favorites, productId] };
        }
    case 'CLEAR_CART':
    return {
        ...state,
        cart: [],};
    default:
    return state;
  }
};

export default rootReducer;
