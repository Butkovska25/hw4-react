export const setProducts = (products) => ({
    type: 'SET_PRODUCTS',
    payload: products,
  });

  export const addToCart = (product) => {
    return {
      type: 'ADD_TO_CART',
      payload: product,
    };
  };

  export const removeFromCart = (productId) => ({
    type: 'REMOVE_FROM_CART',
    payload: productId,
  });

  export const openModal = () => {
    return {
      type: 'OPEN_MODAL',
    };
  };
  
  export const closeModal = () => {
    return {
      type: 'CLOSE_MODAL',
    };
  };

  export const toggleFavorite = (productId) => ({
    type: 'TOGGLE_FAVORITE',
    payload: productId,
  });

  export const clearCart = () => ({
    type: 'CLEAR_CART',
  });