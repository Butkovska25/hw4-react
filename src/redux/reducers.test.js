import rootReducer from './reducers';

describe('Root reducer', () => {
  it('sets products', () => {
    const action = { type: 'SET_PRODUCTS', payload: [{ id: 1, name: 'Product 1' }] };
    const state = rootReducer(undefined, action);
    expect(state.products).toEqual([{ id: 1, name: 'Product 1' }]);
  });

  it('adds product to cart', () => {
    const action = { type: 'ADD_TO_CART', payload: { id: 1, name: 'Product 1' } };
    const prevState = { cart: [] };
    const state = rootReducer(prevState, action);
    expect(state.cart).toEqual([{ id: 1, name: 'Product 1' }]);
  });

  it('removes product from cart', () => {
    const action = { type: 'REMOVE_FROM_CART', payload: 1 };
    const prevState = { cart: [{ id: 1, name: 'Product 1' }] };
    const state = rootReducer(prevState, action);
    expect(state.cart).toEqual([]);
  });

  it('opens modal', () => {
    const action = { type: 'OPEN_MODAL' };
    const prevState = { isModalOpen: false };
    const state = rootReducer(prevState, action);
    expect(state.isModalOpen).toBe(true);
  });

  it('closes modal', () => {
    const action = { type: 'CLOSE_MODAL' };
    const prevState = { isModalOpen: true };
    const state = rootReducer(prevState, action);
    expect(state.isModalOpen).toBe(false);
  });

  it('toggles favorite', () => {
    const action1 = { type: 'TOGGLE_FAVORITE', payload: 1 };
    const prevState1 = { favorites: [] };
    const state1 = rootReducer(prevState1, action1);
    expect(state1.favorites).toEqual([1]);

    const action2 = { type: 'TOGGLE_FAVORITE', payload: 1 };
    const prevState2 = { favorites: [1] };
    const state2 = rootReducer(prevState2, action2);
    expect(state2.favorites).toEqual([]);
  });

  it('clears cart', () => {
    const action = { type: 'CLEAR_CART' };
    const prevState = { cart: [{ id: 1, name: 'Product 1' }] };
    const state = rootReducer(prevState, action);
    expect(state.cart).toEqual([]);
  });
});
